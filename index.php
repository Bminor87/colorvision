<?php
	$highscore = 0;
	if (isset($_POST['score']))
    {
    	setcookie('score', $_POST['score'], time() + (86400 * 365), '/');
        $highscore = $_COOKIE['score'];
    }
   	if ( isset($_COOKIE['score']) )
    {
    	$highscore = $_COOKIE['score'];
    }
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Color Vision Test</title>
<script   src="https://code.jquery.com/jquery-3.2.1.min.js"   integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="   crossorigin="anonymous"></script>
<style type="text/css">
	section {
		font-weight: bold;
		font-size: 1.2em;
		line-height: 20px;
		margin: 20px 0;
		color: #303030;
	}
	h1, p {
		color: #303030;
	}
	section .value {
		font-weight: 400;
	}
	table {
		border: 2px solid black;
		width: 400px;
		height: 400px;
		background-color: #CDCBCB;
	}
	td {
		background: #000000;
	}
	tr td div {
		background: #ff0000;
		width: 100%;
		height: 100%;
		cursor: pointer;
	}
	#test tr td div.correct {
		background: #ee0000;
	}
	#test button {
		position: relative;
		left: 150px;
		height: 60px;
		width: 100px;
		font-weight: bold;
		font-size: 1.5em;
		cursor: pointer;
	}
	#right, #wrong {
		display: none;
		position: absolute;
		top: 200px;
		left: 300px;
	}
	#right img, #wrong img {
		width: 50px;
	}
</style>
<script>
	$(document).ready(function(){
		var start = function() {
			
			// Initialize Game after Game over
			var initialize = function() {
				var new_score = $('#score .value').text();
				if($('#highscore .value').text() < new_score)
				{
					alert('New Highscore!');
					$('#highscore .value').text(new_score);
					$.post('index.html', 'score='+new_score);
				}
				var html = '<tr><td><button>START</button></td></tr>';
				$('#test').html(html);
			}
			
			// TIMER FUNCTION
			var timer = function(time)
			{
				setTimeout(function(){
				$('#time .value').text(time);
				time = time - 1;
				if (time === -1)
				{
					alert('Game Over');
					initialize();
					return;
				}	
				timer(time);
				}, 1000);
			}
			
			// LEVEL FUNCTION
			var level = function(row_num, sub_color)
			{
				// LET'S SET THE LEVEL VARIABLES
				
				var html = '';
				
				var rand_row = Math.floor(Math.random() * row_num) + 1;
				var rand_col = Math.floor(Math.random() * row_num) + 1;
				
				var rand_red	 = Math.floor(Math.random() * 220) + 30;
				var rand_green = Math.floor(Math.random() * 220) + 30;
				var rand_blue = Math.floor(Math.random() * 220) + 30;
				
				if (sub_color < 3)
				{
					sub_color = 3;
				}
				
				var rgb = 'rgb(' + rand_red + ',' + rand_green + ',' + rand_blue + ')';
				var rgb_correct = 'rgb(' + (rand_red-sub_color) + ',' + (rand_green-sub_color) + ',' + (rand_blue-sub_color) + ')';
				
				// LET THE LEVEL BEGIN!
				
				for(row = row_num; row > 0; row--)
				{
					html += "<tr>";
					for(col = row_num; col > 0; col--)
					{
						html += "<td><div></div></td>\n";
					}
					html += "</tr>\n";
				}
				$('#test').html(html);
				
				$('#test tr td div').css('background', rgb);
				
				$('#test tr:nth-child('+rand_row+') td:nth-child('+rand_col+') div').addClass('correct');
				$('#test tr td div.correct').css('background', rgb_correct);
			} // End game()
			
			// START THE GAME
			$('#test button').fadeOut('slow');
			
			var time = 60; // set the timer
			timer(time); // start the timer
			
			var score = 0; // set the score
			$('#score .value').text(score); // Display resetted score
			
			var rows = 3;
			var shade_diff = 25;
			
			level(rows, shade_diff); // Start level 1
			
			$('#test').delegate('div', 'click', function() {
				if($(this).hasClass('correct'))
				{
					$('#right').fadeIn(50).delay(100).fadeOut(50);
					rows += 0.4;
					shade_diff -= 0.6;
					score += 1;
				}
				else
				{
					$('#wrong').fadeIn(50).delay(100).fadeOut(50);
				}
				$('#score .value').text(score);
				
				level(Math.floor(rows), Math.floor(shade_diff));
			});
			
			
			
			
			
		} // End start()
		
		$('#test').delegate('button', 'click', start);
		
	});
</script>
</head>

<body>
<h1>Color Vision Test</h1>
<p>Test your color vision by clicking the box that is slightly different shade than the other boxes</p>
<nav><section id="highscore">High Score: <span class="value"><?= $highscore ?></span></section><section id="time">Time: <span class="value">0</span></section><section id="score">Score: <span class="value">0</span></section></nav>
<table id="test">
	<tr><td><button>START</button></td></tr>
</table>
<figure id="right"><img src="right.png" alt="right" /></figure><figure id="wrong"><img src="wrong.png" alt="wrong" /></figure>
</body>
</html>
